//
// Created by anna on 04.10.2019.
//
#include <iostream>
#include <vector>
#include <GL/glu.h>
#include "textures.h"

using namespace std;

void Textures::loadBMPForTexture(string pathToImg, TextureDatas &textureDatas) {
    //Data read from the header of the BMP file
    unsigned char header[54];
    unsigned int dataPos;
    unsigned int imageSize;

    char *pathToImg_chr = &pathToImg[0];
    FILE * file = fopen(pathToImg_chr,"rb");
    if (!file) {
        cout << "Error loading texture: " << pathToImg << endl;
        return;
    }

    if (fread(header, 1, 54, file) != 54) {
        cout << "Invalid BMP file: " << pathToImg << endl;
        return;
    }

    if (header[0] != 'B' || header[1] != 'M'){
        cout << "Invalid BMP file: " << pathToImg << endl;
        return;
    }

    //Read the necessary data
    dataPos = *(int*)&(header[0x0A]);
    imageSize = *(int*)&(header[0x22]);
    textureDatas.width = *(int*)&(header[0x12]);
    textureDatas.height = *(int*)&(header[0x16]);

    //Fix zero fields
    if (imageSize == 0)
        imageSize = textureDatas.width * textureDatas.height * 3;
    if (dataPos == 0)
        dataPos = 54;

    //Buffer creation
    textureDatas.data = new unsigned char [imageSize];
    fread(textureDatas.data, 1, imageSize, file);
    fclose(file);
    textureDatas.quadrObj = gluNewQuadric();
}

void Textures::createTextures() {
    //Create all texture for OpenGL
    glGenTextures(amount, textures);
    int idTex = 0;

    for (auto &tex:texturesDatas) {
        glBindTexture(GL_TEXTURE_2D, textures[idTex]);
        gluBuild2DMipmaps(GL_TEXTURE_2D, 3, tex.width, tex.height, GL_RGB, GL_UNSIGNED_BYTE, tex.data);
        gluQuadricTexture(tex.quadrObj, GL_TRUE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, tex.width, tex.height, 0, GL_BGR, GL_UNSIGNED_BYTE, tex.data);
        idTex++;
    }
}

void Textures::init() {
    //Determination of material properties
    glMaterialfv (GL_FRONT_AND_BACK, GL_AMBIENT, global::mat_amb);
    glMaterialfv (GL_FRONT_AND_BACK, GL_SPECULAR, global::mat_spec);
    glMaterialf(GL_FRONT,GL_SHININESS, global::shininess);

    //Determination of lighting properties
    //   glLightfv(GL_LIGHT0, GL_DIFFUSE, global::light_col);
//    glEnable(GL_LIGHTING);
//    glEnable(GL_LIGHT0);

    //Removal of invisible lines and surfaces
    glEnable(GL_DEPTH_TEST);

    //Normalization of normals
    glEnable(GL_NORMALIZE);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
}

void Textures::loadTextures(string &path) {
    TextureDatas textureDatas;
    vector<string> texturesPath;

    texturesPath.emplace_back(path + "/image/background.bmp");
    texturesPath.emplace_back(path + "/image/texture2.bmp");
    texturesPath.emplace_back(path + "/image/start.bmp");
    texturesPath.emplace_back(path + "/image/stop.bmp");

    for (auto &pathToImg:texturesPath){
        loadBMPForTexture(pathToImg, textureDatas);
        texturesDatas.emplace_back(textureDatas);
    }

    createTextures();
    init();
}