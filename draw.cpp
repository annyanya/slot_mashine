//
// Created by anna on 04.10.2019.
//
#include <iostream>
#include <vector>
#include <GL/glu.h>
#include "constantList.h"
#include "draw.h"

using namespace std;

void Draw::orthogonalStart()
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    glOrtho(-(float)global::winWidth/2, (float)global::winWidth/2, -(float)global::winHeight/2, (float)global::winHeight/2, -1.0, 1.0);
    gluPerspective(0.01, 1.0, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
}

void Draw::orthogonalEnd()
{
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}

void Draw::drawRect(int textureId, int x, int y, int w, int h) {
    glBindTexture(GL_TEXTURE_2D, Textures::textures[textureId]);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-(float)global::winWidth/2, (float)global::winWidth/2, -(float)global::winHeight/2, (float)global::winHeight/2, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glBegin(GL_QUADS);
    glTexCoord2f(0.0,1.0); glVertex2f(x, y);
    glTexCoord2f(0.0,0.0); glVertex2f(x, y - h);
    glTexCoord2f(1.0,0.0); glVertex2f(x + w, y - h);
    glTexCoord2f(1.0,1.0); glVertex2f(x + w, y);
    glEnd();

    glDisable(GL_TEXTURE_GEN_S);
}

void Draw::drawBackground() {
    drawRect(0, -global::winWidth/2, global::winHeight/2, global::winWidth, global::winHeight);
}

void Draw::drawButtonStart() {
    drawRect(2, -350, -300, 150, 90);
}

void Draw::drawButtonStop() {
    drawRect(3, 200, -300, 150, 90);
}

void Draw::drawReel(Reel &reel) {
    glBindTexture(GL_TEXTURE_2D, Textures::textures[1]);
    gluCylinder(texturesDatas[1].quadrObj, reel.radius, reel.radius, reel.height, 20, 1);
}

void Draw::drawAll(vector<Reel> &reels) {
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    orthogonalStart();

    //Create five reels
    for (int i = -2; i <= 2; i++) {
        glPushMatrix();
        glTexCoord2f(0, 1);
        glTranslatef(0.0, 0.0, (float)i * reels[i+2].height - reels[i+2].height/2);
        glRotatef(reels[i+2].angle , 0, 0, 1);
        drawReel(reels[i+2]);
        glPopMatrix();
    }

    drawButtonStart();
    drawButtonStop();
    drawBackground();

    glDisable(GL_TEXTURE_2D);
    orthogonalEnd();
}
