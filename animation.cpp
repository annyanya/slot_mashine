//
// Created by anna on 05.10.2019.
//
#include <iostream>
#include <vector>
#include <GL/glu.h>
#include <GL/glut.h>
#include <chrono>
#include "animation.h"

using namespace std;

void Animation::start(string pathToFolder) {

    rotateReels = false;
    alignment = 0.0f;
    stopNow = 0; //number of stopped drums
    revCounter = global::speedStop*(global::amountReels + 2); //stop motion counter
    count = 0;
    Textures::loadTextures(pathToFolder);

    float startRotateAngle = 0.0f;

    for (int i = 0; i < global::amountReels; i++){
        startRotateAngle = (float)(global::sector*i);
        Reel newReel(i, global::reelRad, global::reelHeight, startRotateAngle);
        reels.push_back(newReel);
    }
}

void Animation::display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(35.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glPushMatrix();
    Draw::drawAll(reels);
    glutSwapBuffers();
}

void Animation::update() {
    //Increase the current rotation angle
    if (rotateReels) {
        for (int i = 0; i < 5; i++) {
            reels[i].angle = reels[i].angle + (float) (i + 1) * 2 - global::minRotationSpeed;
        }
        stopTimer = std::chrono::steady_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::seconds>(stopTimer - startTimer).count();
        if (duration >= global::timeToStop) {
            stopNow ++;
            rotateReels = false;
        }
    } else {
        //Stop the drums one at a time
        if (stopNow > 0 && stopNow <= global::amountReels) {
            int x = global::amountReels - stopNow;
            for (int i = 0; i < x; i++) {
                reels[i].angle = reels[i].angle + (float) (i + 1) * 2 - global::minRotationSpeed;
            }

            revCounter--;

            if (revCounter == (x + 2) * global::speedStop - 1) {
                alignment = (int)reels[x].angle%global::sector;
                reels[x].angle = reels[x].angle -  alignment;
            }

            if (count <  global::speedStop / 2) {
                reels[x].angle = reels[x].angle +  global::stopShift;
            }

            if (count > global::speedStop / 2) {
                reels[x].angle = reels[x].angle -  global::stopShift;
            }

            if (revCounter == (x + 1) * global::speedStop) {
                stopNow++;
                count = 0;
            }
        }
    }
    //Scene update
    glutPostRedisplay();
}

void Animation::mouseClick(int button, int state, int x, int y) {
    //Only at the beginning of the movement if the left button is pressed
    if (button == GLUT_LEFT_BUTTON) {
        //When the button is released
        if (state == GLUT_UP) {
            if (x >= 50 && x <= 200 && y >= 700 && y <= 790) {
                startTimer = std::chrono::steady_clock::now();
                stopNow = 0;
                revCounter = global::speedStop*(global::amountReels + 2);
                rotateReels = true;
            }

            if (x >= 600 && x <= 850 && y >= 700 && y <= 790) {
                if (rotateReels) {
                    stopNow ++;
                }
                rotateReels = false;
            }
        }
    }
}