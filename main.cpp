#include <iostream>
#include <vector>
#include <GL/glut.h>
#include <chrono>
#include "constantList.h"
#include "animation.h"

using namespace std;

Animation animation;

void wrapDisplay() {
    animation.display();
}

void wrapUpdate() {
    animation.update();
}

void wrapMouseClick(int button, int state, int x, int y) {
    animation.mouseClick(button, state, x, y);
}

int main(int argc, char **argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(global::winWidth, global::winHeight);
    glutCreateWindow("Casino");

    animation.start(argv[1]);

    glutDisplayFunc(wrapDisplay);
    glutIdleFunc(wrapUpdate);
    glutMouseFunc(wrapMouseClick);
    glutMainLoop();

    return 0;
}