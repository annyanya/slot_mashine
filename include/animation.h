//
// Created by anna on 05.10.2019.
//
#include "draw.h"
#ifndef SLOT_MASHINE_ANIMATION_H
#define SLOT_MASHINE_ANIMATION_H

class Animation: public Draw {
public:
    std::vector <Reel> reels;

    bool rotateReels;
    int alignment;
    int stopNow; //number of stopped drums
    int revCounter; //stop motion counter
    int count;

    std::chrono::steady_clock::time_point startTimer;
    std::chrono::steady_clock::time_point stopTimer;

    using Draw::Draw;

    void start(std::string pathToFolder);
    void display();
    void update();
    void mouseClick(int button, int state, int x, int y);
};



#endif //SLOT_MASHINE_ANIMATION_H
