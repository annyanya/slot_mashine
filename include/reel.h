//
// Created by anna on 04.10.2019.
//
#ifndef SLOT_MASHINE_REEL_H
#define SLOT_MASHINE_REEL_H

class Reel {
public:
    int id;
    float radius;
    float height;
    float angle;

    Reel(int id, float radius, float height, float angle) {
        this->id = id;
        this->radius = radius;
        this->height = height;
        this->angle = angle;
    }
};


#endif //SLOT_MASHINE_REEL_H
