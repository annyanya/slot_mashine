//
// Created by anna on 04.10.2019.
//
#pragma once

namespace global {
    const int winWidth = 800;
    const int winHeight = 800;

    const float reelRad = 0.8, reelHeight = 0.45;

    const int amountTextures = 4; //4 = number of textures
    const float diff_1[] = {0.8, 0.8, 0.8};
    const float diff_2[] = {0.3,0.3,0.9};
    const float mat_amb[] = {0.2,0.2,0.2};
    const float mat_spec[] = {0.6,0.6,0.6};
    const float shininess = 0.7 * 128.0;

    const int amountReels = 5; //number of reels
    const float minRotationSpeed = 15.0f; //minimum rotation speed
    const int sector = 40; //360 degrees divided by the number of sectors
    const int speedStop = 4;
    const int stopShift = 4; //shift after stop
    const int timeToStop = 4; //auto stop (seconds)
}
