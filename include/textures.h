//
// Created by anna on 04.10.2019.
//
#include "constantList.h"
#ifndef SLOT_MASHINE_TEXTURE_H
#define SLOT_MASHINE_TEXTURE_H

struct TextureDatas {
    std::string path;
    unsigned int width;
    unsigned int height;
    unsigned char * data;
    GLUquadricObj *quadrObj;
};

class Textures {
public:
    int amount = global::amountTextures;
    std::vector<TextureDatas> texturesDatas;
    GLuint textures[global::amountTextures];

//    Textures(){}
    void loadTextures(std::string &path);

private:
    void loadBMPForTexture(std::string pathToImg, TextureDatas &textureData);
    void createTextures();
    void init();
};

#endif //SLOT_MASHINE_TEXTURE_H
