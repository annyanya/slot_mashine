//
// Created by anna on 04.10.2019.
//
#include "reel.h"
#include "textures.h"
#ifndef SLOT_MASHINE_DRAWING_H
#define SLOT_MASHINE_DRAWING_H

class Draw: public Textures {
public:
    using Textures::Textures;
    void drawAll(std::vector<Reel> &reels);

private:
    void orthogonalStart();
    void orthogonalEnd();
    void drawRect(int textureId, int x, int y, int w, int h);
    void drawBackground();
    void drawButtonStart();
    void drawButtonStop();
    void drawReel(Reel &reel);
};


#endif //SLOT_MASHINE_DRAWING_H
